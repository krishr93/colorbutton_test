import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

export const replaceCamelWithSpaces = (colorName) => {
    return colorName.replace(/\B([A-Z])\B/g, ' $1');
}

function App() {

  const [textColor, setTextColor] = useState('MediumVioletRed');
  const [text, setText] = useState('Change to MidnightBlue');
  const [disabled, setDisabled] = useState(false);

  const buttonClick = (color) => {
      console.log("color : ", color);
      if(color === 'MediumVioletRed') {
        setTextColor('MidnightBlue');
        setText('Change to MediumVioletRed');
      } else {
        setTextColor('MediumVioletRed');
        setText('Change to MidnightBlue');
      }
  };

  const checkboxClick = (disable) => {
    console.log("0) checkboxClick : disable - ", disable);
    console.log("0) text : ", text);
    if(disable){
      setTextColor('gray');
      setDisabled(true);
    } else if(disable === false && text === 'Change to MidnightBlue') {

      setTextColor('MediumVioletRed');
      //buttonClick('MediumVioletRed');
      setDisabled(false);
    } else if(disable === false && text === 'Change to MediumVioletRed') {
      setTextColor('MidnightBlue');
      //buttonClick('blue');
      setDisabled(false);
    }

  };
  
  return (
    <div>
      <button style={{backgroundColor : textColor}} 
              onClick={() => buttonClick(textColor)} 
              disabled={disabled}
      >
          {text}
      </button>

      <input type="checkbox" 
             onChange={(e) => checkboxClick(e.target.checked)}
             id="disable-button-checkbox"
             defaultChecked={disabled}
             // Need to investigate 
             aria-checked={disabled}
      />
      <label htmlFor="disable-button-checkbox">Disable button</label>

    </div>
  );
}

export default App;
