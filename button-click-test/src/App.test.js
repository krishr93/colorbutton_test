import { render, screen, fireEvent } from '@testing-library/react';
import App, { replaceCamelWithSpaces } from './App';

test('Button has correct initial color', () => {

  // 1) Rendering a component
  render(<App />);

  // 2.1) Finding an element 
  // https://www.w3.org/TR/wai-aria/#role_definitions
  // Find an element with a ROLE of button and text of 'Change to MidnightBlue'
  const colorButton = screen.getByRole('button', {name: 'Change to MidnightBlue'});
  
  // 3) Assertion: Expecting to have something
  // https://github.com/testing-library/jest-dom
  // expect the background color to be MediumVioletRed.
  // https://github.com/testing-library/jest-dom#tohavestyle
  // Syntax ==> assertion(obj).customMatcher(argument)
  expect(colorButton).toHaveStyle({backgroundColor: 'MediumVioletRed'})

  // 2.2) Interating with something 
  // Click button --> Interacting with the DOM.
  // fireEvent : it used for us to interact with Virtual DOM 
  fireEvent.click(colorButton);

  // 3) Assertion - on style
  // expect the background color to be MidnightBlue
  expect(colorButton).toHaveStyle({backgroundColor: 'MidnightBlue'});

  // 3) Assertion - on text
  expect(colorButton.toHaveTextContent).toBe('Change to MediumVioletRed');
});

test('initial conditions', () => {

  // 1)
  render(<App />);

  // Check that the button starts out enabled
  // 2 - https://www.w3.org/TR/wai-aria/#role_definitions
  const colorButton = screen.getByRole('button', {name: 'Change to MidnightBlue'});
  // 3 - Ref: https://github.com/testing-library/jest-dom#tobeenabled
  expect(colorButton).toBeEnabled();
  
  // Check that the checkbox starts out unchecked
  // 2 - https://www.w3.org/TR/wai-aria/#role_definitions
  const checkbox = screen.getByRole('checkbox');
  // 3 - https://github.com/testing-library/jest-dom#tobeenabled
  expect(checkbox).not.toBeChecked();

})

test('checkbox disables button on 1st click & emables button on 2nd click', () => {

  // 1) render the component
  render(<App />);

  // 2) Accessing the checkbox & doing the activity
  const checkbox = screen.getByRole('checkbox', {name: 'Disable button'});
  const colorButton = screen.getByRole('button', {name: 'Change to MidnightBlue'});

  // 3) assertion
  fireEvent.click(checkbox);
  expect(colorButton).toBeDisabled();

  fireEvent.click(checkbox);
  expect(colorButton).toBeEnabled();

})

test('disable button has gray background and revert to MediumVioletRed', () => {
  // 1)
  render(<App />);

  // 2.1)
  const checkbox = screen.getByRole('checkbox', {name: 'Disable button'});
  const colorButton = screen.getByRole('button', {name: 'Change to MidnightBlue'});
  // 2.2)
  fireEvent.click(checkbox);

  // 3) 
  expect(colorButton).toHaveStyle({backgroundColor: 'gray'});
  expect(colorButton).toBeDisabled();

  // 2.2)
  fireEvent.click(checkbox);

  // 3)
  expect(colorButton).toBeEnabled();
  expect(colorButton).toHaveStyle({backgroundColor: 'MediumVioletRed'});
})

test('click disabled button has gray background and revert to MidnightBlue', () => {
  // 1)
  render(<App />);

  // 2.1)
  const colorButton = screen.getByRole('button', {name: 'Change to MidnightBlue'})
  const checkbox = screen.getByRole('checkbox', {name: 'Disable button'});
  // 2.2)
  fireEvent.click(colorButton);

  // 3) 
  expect(colorButton).toHaveStyle({backgroundColor: 'MidnightBlue'});

  // 2.2)
  fireEvent.click(checkbox);

  // 3)
  expect(colorButton).toHaveStyle({backgroundColor: 'gray'});

  // 2.2)
  fireEvent.click(checkbox);
  expect(colorButton).toHaveStyle({backgroundColor: 'MidnightBlue'});
})

// Medium Violet MediumVioletRed
// Midnight MidnightBlue
describe('space before camel-case capital letters', () => {

  test('works for no inner capital letters, ', () => {
    // 2.1 & 3 combine 
    expect(replaceCamelWithSpaces('Red')).toBe('Red');
  });

  test('works for one inner capital letter', () => {
    // 2.1 & 3 combine 
    expect(replaceCamelWithSpaces('MidnightBlue')).toBe('Midnight Blue');
  });

  test('works for multiple inner capital letters', () => {
    // 2.1 & 3 combine 
    expect(replaceCamelWithSpaces('MediumVioletRed')).toBe('Medium Violet Red');
  });
})